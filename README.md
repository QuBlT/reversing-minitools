# Reversing Minitools

A really early mini toolkit usefull for reversing, exploiting and debuging pruposes. 


## inreturn

inreturn sirve para pasar a shellcode direcciones de memoria e invertirlas para exploiting

### Uso: 
```
inreturn [Dirección de retorno]"

inreturn 53127488"
```
Output:
```
Normal:    \x53\x12\x74\x88 
Invertido:   \x88\x74\x12\x53

```


## revstring

Inverte cadenas de texto y te las devuele shellcode 


### Uso:
```
revstring [string]

revstring Hola
```
Output:

```
Hex inverso: 		616c6f48
Shell code inverso: 	\x61\x6c\x6f\x48
```

## nasmtoHexstring

### Uso:

```
nasmtoHexstring [NAsmFile]"
```


